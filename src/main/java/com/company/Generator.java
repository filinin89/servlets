package com.company;

import com.company.dto.Product;

import java.util.Random;

public class Generator {

    private String images[] = {"img/no-image-found.jpg","img/no-image-found.jpg", "img/no-image-found.jpg", "img/no-image-found.jpg", "img/no-image-found.jpg", "img/no-image-found.jpg"};
    private String names[] = {"Product1", "Product2", "Product3", "Product4", "Product5", "Product6", "Product7"};
    private int costs[] = {1243, 676, 9097, 2432, 11434, 8999, 7152};
    private Random random = new Random();
    private final int NUMBER_OF_PRODUCTS = 20;

    public Product[] generateProducts() {

        Product products[] = new Product[NUMBER_OF_PRODUCTS];

        for(int i=0; i<NUMBER_OF_PRODUCTS; i++){
            Product product = new Product();
            product.setName(names[random.nextInt(names.length)]);
            product.setImg(images[random.nextInt(images.length)]);
            product.setCost(costs[random.nextInt(costs.length)]);
            product.setId(i);

            products[i] = product;
        }

        return products;
    }
}
