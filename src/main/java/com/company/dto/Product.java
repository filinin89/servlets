package com.company.dto;

public class Product {

    private String img;
    private String name;
    private int cost;
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }




    public void setImg(String img) {
        this.img = img;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }



    public String getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public int getCost() {
        return cost;
    }



}
