package com.company.servlets;

import com.company.Generator;
import com.company.MyListenerClass;
import com.company.dto.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class IndexServlet extends HttpServlet
{
    private Logger logger = LoggerFactory.getLogger(MyListenerClass.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Generator gen= new Generator();
        Product[] products = gen.generateProducts();
        getServletContext().setAttribute("Products", products); // добавим список товаров в контекст, теперь jsp страница может работать с ним
        request.getRequestDispatcher("/index.jsp").forward(request, response);

        HttpSession session = request.getSession();
        logger.info("Id сессии " + session.getId());

    }
}
