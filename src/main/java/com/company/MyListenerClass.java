package com.company;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class MyListenerClass implements ServletContextListener, HttpSessionListener {


    private Logger logger = LoggerFactory.getLogger(MyListenerClass.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.info("Context Initialized");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }



    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        logger.info("Сессия создана");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        logger.info("Сессия разрушена");
    }
}
